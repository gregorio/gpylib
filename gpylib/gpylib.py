def my_gpylib_hello():
    print('Hello gpylib!')

def compare_values(value1, value2):
    if value1 > value2:
        print('{} is greater than {}.'.format(value1, value2))
    else:
        print('{} is not greater than {}.'.format(value1, value2))

class Fish():
    def __init__(self, name='Roberto', last_name='Pérez'):
        self.name = name
        self.last_name = last_name
    
    def my_name(self):
        print('{} {}'.format(self.name, self.last_name))