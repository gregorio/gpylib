from setuptools import setup

setup(
    name='gpylib',
    packages=['gpylib'],
    version='0.1',
    description='This is a library to run some tests',
    url='https://git.disroot.org/gregorio/gpylib',
    author='gregorio',
    author_email='gregorio@disroot.org',
    py_modules=['gpylib'],
    long_description=open('README.md').read(),
    license='MIT',
    install_requires=['numpy'],
    )